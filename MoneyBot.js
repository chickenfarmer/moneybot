var MoneyBot = (function()
{
	// Determines the current score of the room and checks to see that the song doesn't
	// have more than 55% negative votes.
	var scoreCheck = function()
	{
		var votes = API.getRoomScore();
		
		if (votes.hasOwnProperty('positive') && votes.hasOwnProperty('negative'))
		{
			var totalVotes = new Number(votes.positive + votes.negative);
			
			if (totalVotes > 0)
			{
				var pos = new Number(votes.positive / totalVotes);
				var neg = new Number(votes.negative / totalVotes);
			}
			
			else
			{
				var pos = new Number(0);
				var neg = new Number(0);
			}
			
			if (pos < neg)
			{
				if (neg > 0.55)
				{
					API.sendChat("Your song is at or above 55% negative votes and is being skipped.");
				}
				
				else
				{
					API.sendChat("Your song is negative in score. At 55% negative votes, it will be skipped automatically.");
				}
			}
		}
	};
	
	var initScoreCheck = function(timer)
	{
		window.setInterval(scoreCheck, timer);
	};
	
	return {
		scoreCheck: initScoreCheck,
		
	}
})();

MoneyBot.scoreCheck(5000);